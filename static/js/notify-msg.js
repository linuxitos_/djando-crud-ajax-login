$(document).on("click", ".print-ticket", function () {
	var NWin = window.open($(this).prop('href'), 'Visualizar ticket', 'height=850,width=800,resizable=no,left=0, top=0');
	if (window.focus){
		NWin.focus();
	}
	return false;
});

$(".sprint-ticket").click(function() {
	var NWin = window.open($(this).prop('href'), 'Visualizar ticket', 'height=850,width=800,resizable=no,left=0, top=0');
	if (window.focus){
		NWin.focus();
	}
	return false;
});

$(document).on("keypress", ".decimal", function (event) {
	return decimal(event, this);
});

function decimal(evt, element) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode != 13 && (charCode != 46 || $(element).val().indexOf('.') != -1) &&
		(charCode < 48 || charCode > 57)){
		return false;
	}else{
		return true;
	}
}

$(document).on("keypress", ".enteros", function (event) {
	return enteros(event, this);
});

function enteros(evt, element) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode != 13 && (charCode < 48 || charCode > 57)){
		return false;
	}else{
		return true;
	}
}

$(document).on("click", ".btn-opt-toggle", function (e) {
	var currenttoggle = ""
	if($('body').hasClass("sidenav-toggled")){
		currenttoggle = 'sidenav-toggled';
	}else{
		currenttoggle = '';
	}
	$.ajax({
		type: 		"POST",
		url: 		"ajax/actions_users/optToggle.php",
		dataType: 	"JSON",
		data: {
			class : currenttoggle,
		},
		beforeSend: function(objeto) {
			$('#div-cnt-cats').html('');
		},
		success: function(datos) {
			//$("#div-cnt-cats").html('');
			//notify_msg(datos.icon, " ", datos.msg, "#", datos.tipo);
		},
		error: function(response) {
			$("#div-cnt-cats").html('');
		}
	});
	e.preventDefault();
});


function mensaje_modal(message, tipo, icono, titulo, url, elemento) {
	$.notify({
		// options
		icon: icono,
		title: titulo,
		message: message,
		url: url,
		target: '_blank'
	},{
		// settings
		element: 'div#'+elemento,
		position: null,
		type: tipo,
		allow_dismiss: true,
		newest_on_top: false,
		showProgressbar: false,
		placement: {
			from: "top",
			align: "center"
		},
		offset: {
			x: 10,
			y: 60
		},
		spacing: 10,
		z_index: 1031,
		delay: 5000,
		timer: 1000,
		url_target: '_blank',
		mouse_over: null,
		animate: {
			enter: 'animated fadeInDown',
			exit: 'animated fadeOutUp'
		},
		onShow: null,
		onShown: null,
		onClose: null,
		onClosed: null,
		icon_type: 'class',
		template: '<div data-notify="container" class="col-xs-10 col-md-10 col-sm-10 alert alert-{0}" role="alert">' +
			'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
			'<span data-notify="icon"></span> ' +
			'<span data-notify="title">{1}</span> ' +
			'<span data-notify="message">{2}</span>' +
			'<div class="progress" data-notify="progressbar">' +
				'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
			'</div>' +
			'<a href="{3}" target="{4}" data-notify="url"></a>' +
		'</div>' 
	});
}

function notify_msg(icono, titulo, mensaje, link, tipo) {
	$.notify({
		// options
		icon: icono,
		title: "<strong>"+titulo+" </strong>",
		message: mensaje,
		url: link,
		target: '_blank'
	},{
		// settings
		element: 'body',
		position: null,
		type: tipo,
		allow_dismiss: true,
		newest_on_top: false,
		showProgressbar: false,
		placement: {
			from: "top",
			align: "right"
		},
		offset: 20,
		spacing: 10,
		z_index: 1031,
		delay: 5000,
		timer: 1000,
		url_target: '_blank',
		mouse_over: 'pause',
		animate: {
			enter: 'animated fadeInDown',
			exit: 'animated fadeOutUp'
		},
		onShow: null,
		onShown: null,
		onClose: null,
		onClosed: null,
		icon_type: 'class',
		template: '<div data-notify="container" class="col-xs-11 col-sm-3 col-xs-2 col-2 alert alert-notify alert-dismissible alert-{0}" role="alert">' +
			'<span data-notify="icon"></span> ' +
			'<span data-notify="title">{1}</span> ' +
			'<span data-notify="message">{2}</span>' +
			'<div class="progress" data-notify="progressbar">' +
				'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
			'</div>' +
			'<a href="{3}" target="{4}" data-notify="url"></a>' +
			'<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' +
		'</div>',
	});
}