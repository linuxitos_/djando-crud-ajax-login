// Write your Javascript here
var limite 		= 10;
var filter 		= 1;
var order 		= "-";
var order_by	= "id";

$('.dropdown-limit').find('a').click(function(e) {
	$("#spn-show-list").text($(this).data("total"));
	limite = $(this).data("total");
	load(1);
	e.preventDefault();
});

$('.dropdown-edo').find('a').click(function(e) {
	filter 		= $(this).data("edo");
	$("#i-icon-act").removeAttr("class").attr("class", $(this).data("icon"));
	$("#spn-desc-act").text($(this).data("desc"));
	load(1);
	e.preventDefault();
});

$("#form-search").submit(function( event ) {
	//var parametros = $(this).serialize();
	load(1);
	event.preventDefault();
});

function load(page) {
	var search = $('#txt-search').val();
	var csrftoken = $("[name=csrfmiddlewaretoken]").val();
	$.ajax({
		type: 'POST',
		url: 'load',
		method: 'POST',
		dataType: 'JSON',
		data: {
			csrfmiddlewaretoken: csrftoken,
			page: page,
			search: search,
			filter: filter,
			limite: limite,
			order: order,
			order_by: order_by,
		},
		beforeSend: function (objeto) {
			$('#btn-search').html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Buscando...');
			$("#div-cnt-load").html('<div class="text-center alert alert-info" role="alert">' +
				'<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Buscando...</div>');
		},
		success: function (response) {
			$('#div-cnt-load').html(response.data);
			$('#h5-cnt-total').html("Total de resultados: "+response.total);
			$('#div-cnt-paginate').html(response.paginate);
			$('#btn-search').html('<i class="bi bi-search"></i>');
		},
		error: function (data) {
			$("#btn-search").html('<i class="bi bi-search"></i>');
			$("#div-cnt-load").html('<div class="text-center alert alert-danger" role="alert"><i class="bi bi-x-circle"></i> Error interno, intenta más tarde.</div>');
		}
	});
}

$(document).on("click", ".table th.th-link", function () {
	if (order == "") {
		order = "-";
	} else {
		order = "";
	}
	order_by = $(this).attr("data-field");
	load(1);
});

$(document).on("click", ".mdl-del-reg", function () {
	$("#txt-id-reg-del").val($(this).data('idreg'));
	$("#txt-nom-reg").text('"' + $(this).data('nomreg') + '"');

	$("#div-cnt-img-ajx").html("");
});

$(document).on("click", ".mdl-add-reg", function () {
	$("#form-add-post")[0].reset();
	$("#div-cnt-img-ajx").html("");
});

$("#form-add-post").submit(function (e) {
	$('#btn-register').attr("disabled", true);
	//var serializedData = $(this).serialize();
	form = $(this)
	var formData = new FormData(this);
	$.ajax({
		method: "POST",
		type: "POST",
		cache: false,
		contentType: false,
		processData: false,
		dataType: "JSON",
		url: "ajax/add",
		//data: serializedData,
		data: formData,
		beforeSend: function (objeto) {
			$("#div-cnt-msg-add-reg").html('<div class="alert alert-info" role="alert">' +
				'<span class="spinner-border spinner-border-sm spin-x" role="status" aria-hidden="true"></span> Guardando...' + '</div>');
			$('#btn-register').html('<span class="spinner-border spinner-border-sm spin-x" role="status" aria-hidden="true"></span> Guardando...');
		},
		success: function (response) {
			$('#btn-register').attr("disabled", false);
			if (response.tipo=="success") {
				$("#btn-close-mdl-add-reg").trigger("click");
				$("#form-add-post")[0].reset();
				load(1);
				notify_msg(response.icon, " ", response.msg, "#", response.tipo);
			}else{
				$("#div-cnt-msg-add-reg").html('<div class="alert alert-' + response.tipo + ' alert-dismissible fade show" role="alert"><i class="' + response.icon + '"></i> ' + response.msg + '</div>');
			}
			$('#btn-register').html('<i class="bi bi-check-circle"></i> Agregar');
			setTimeout(function () {
				$("#div-cnt-msg-add-reg").html("");
			}, 3000);
		},
		error: function (response) {
			console.log(response);
			$('#btn-register').attr("disabled", false);
			$('#btn-register').html('<i class="bi bi-x-circle"></i> Agregar');
			$("#div-cnt-msg-add-reg").html('<div class="alert alert-danger alert-dismissible fade show" role="alert"><i class="bi bi-x-circle"></i> Error interno, intenta más tarde.</div>');
			setTimeout(function () {
				$("#div-cnt-msg-add-reg").html("");
			}, 3000);
		}
	});
	e.preventDefault();
});

$("#form-del-reg").submit(function (event) {
	//var csrftoken = $("[name=csrfmiddlewaretoken]").val();
	$("#btn-del-reg").attr("disabled", true);
	//var idreg = $("#txt-id-reg-del").val();
	var parametros = $(this).serialize();
	$.ajax({
		type: "POST",
		method: "POST",
		url: "delete",
		/*data: {
			csrfmiddlewaretoken: csrftoken,
			list_ids: idreg,
		},*/
		data: parametros,
		dataType: "json",
		beforeSend: function (objeto) {
			$("#btn-del-reg").html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Eliminando');
		},
		success: function (datos) {
			$("#btn-del-reg").html('<i class="bi bi-trash"></i> Eliminar');
			$("#btn-del-reg").attr("disabled", false);
			$("#btn-close-mdl-del-reg").trigger("click");
			notify_msg(datos.icon, " ", datos.msg, "#", datos.tipo);
			if (datos.tipo == "success") {
				load(1);
			}
		},
		error: function (data) {
			$("#form-del-reg")[0].reset();
			$("#btn-del-reg").html('<i class="bi bi-trash"></i> Eliminar');
			$("#btn-del-reg").attr("disabled", false);
			$("#div-cnt-del-reg").html('<div class="alert alert-danger" role="alert"><i class="bi bi-info-circle"></i>' +
				' Error interno, intenta más tarde.</div>');
			setTimeout(function () {
				$("#div-cnt-del-reg").html('');
			}, 3000);
		}
	});
	event.preventDefault();
});

function readURL(input, div) {
	var total_files = $(input)[0].files.length;
	$("#" + div).html('');
	for (i = 0; i < total_files; i++) {
		if ($(input)[0].files[i]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$("#" + div).append('<div class="col-md-12"><img id="img-' + i + '" src="' + e.target.result + '" class="img-fluid"/></div>')
			};
			reader.readAsDataURL(input.files[i]);
		}
	}
}

$("#form-up-profile").submit(function (e) {
	$('#btn-up-profile').attr("disabled", true);
	var parametros = $(this).serialize();
	//form = $(this)
	var formData = new FormData(this);
	$.ajax({
		method: "POST",
		type: "POST",
		dataType: "JSON",
		url: "upprofile",
		cache: false,
		contentType: false,
		processData: false,
		//data: parametros,
		data: formData,
		beforeSend: function (objeto) {
			$("#div-cnt-msg-up-profile").html('<div class="alert alert-info" role="alert">' +
				'<span class="spinner-border spinner-border-sm spin-x" role="status" aria-hidden="true"></span> Guardando...' + '</div>');
			$('#btn-up-profile').html('<span class="spinner-border spinner-border-sm spin-x" role="status" aria-hidden="true"></span> Guardando...');
		},
		success: function (response) {
			$('#btn-up-profile').attr("disabled", false);
			$("#div-cnt-msg-up-profile").html('');
			if (response.tipo == "success") {
				//$("#btn-close-mdl-add-reg").trigger("click");
				//$("#form-up-profile")[0].reset();
				//load(1);
				notify_msg(response.icon, " ", response.msg, "#", response.tipo);
			} else {
				$("#div-cnt-msg-up-profile").html('<div class="alert alert-' + response.tipo + ' alert-dismissible fade show" role="alert"><i class="' + response.icon + '"></i> ' + response.msg + '</div>');
			}
			$('#btn-up-profile').html('<i class="bi bi-check-circle"></i> Agregar');
			setTimeout(function () {
				$("#div-cnt-msg-up-profile").html("");
			}, 3000);
		},
		error: function (response) {
			console.log(response);
			$('#btn-up-profile').attr("disabled", false);
			$('#btn-up-profile').html('<i class="bi bi-x-circle"></i> Agregar');
			$("#div-cnt-msg-up-profile").html('<div class="alert alert-danger alert-dismissible fade show" role="alert"><i class="bi bi-x-circle"></i> Error interno, intenta más tarde.</div>');
			setTimeout(function () {
				$("#div-cnt-msg-up-profile").html("");
			}, 3000);
		}
	});
	e.preventDefault();
});