from app_1 import views as app1
from app_2 import views as app2
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from app_1.views import signup
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),

    # app_1
    #FBV
    path('', app1.index, name = 'index'),
    path('index', app1.index, name='index'),
    #path('', app1.contactPage),
    path('contact', app1.contactPage),
    path('ajax/contact', app1.postContact, name = 'contact_submit'),
    path('add', app1.addPost, name='add'),
    path('ajax/add', app1.ajxAddPost, name='add_post'),
    path('delete', app1.deletePost, name='delete'),
    path('edit/<int:id>', app1.editPost, name='edit'),
    path('view/<slug:slug>', app1.viewPost, name='view_post'),
    
    #CBV
    path('contact_cbv', app1.ContactAjax.as_view(), name = 'contact_submit_cbv'),
    #all posts
    path('posts', app1.userPosts, name='posts'),
    path('load', app1.loadPosts, name='load'),
    path('loadscroll', app1.loadscroll, name='loadscroll'),
    path('profile', app1.profile, name='profile'),
    path('upprofile', app1.upprofile, name='upprofile'),
    
    #auth login
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/signup/', signup.as_view(), name='signup'),
    path('auth', app1.loginUser, name='auth'),
    path('register', app1.registerUser, name='register'),
    path('search', app1.search, name='search'),

    #app_2
    path('user', app2.userPanel, name='user'),
    path('ajax/get_user_info', app2.getUserInfo, name = 'get_user_info'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
