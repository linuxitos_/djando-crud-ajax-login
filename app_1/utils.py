import string
from django.utils.text import slugify
import random

def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))

def unique_slug_generator(instance, new_slug=None):
	if new_slug is not None:
		slug = new_slug
	else:
		slug = slugify(instance.first_name)
	Klass = instance.__class__
	qs_exists = Klass.objects.filter(slug=slug).exists()
	if qs_exists:
		new_slug = "{slug}-{randstr}".format(
			slug=slug, randstr=random_string_generator(size=4))

		return unique_slug_generator(instance, new_slug=new_slug)
	return slug


def paginate(page, tpages, adjacents, fuc_load) :
	prevlabel = '<i class="bi bi-chevron-left"></i>'
	nextlabel = '<i class="bi bi-chevron-right"></i>'
	out = '<nav aria-label="..."><ul class="pagination pagination-large justify-content-end">'

	# previous label
	if page == 1 :
		out += '<li class="page-item disabled"><a class="page-link">'+prevlabel+'</a></li>'
	else:
		if page == 2 :
			out += '<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="'+fuc_load+'(1);">'+prevlabel+'</a></li>'
		else:
			out += '<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="'+fuc_load+'('+str(page-1)+');">'+prevlabel+'</a></li>'
	# first label
	if page > (adjacents+1) :
		out += '<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="'+fuc_load+'(1);">1</a></li>'

	# interval
	if page > (adjacents+2) :
		out += '<li class="page-item disabled"><a class="page-link">...</a></li>'
	
	# pages
	pmin = (page-adjacents) if page > adjacents else 1
	pmax = (page+adjacents) if page < (tpages - adjacents) else tpages

	for i in range(pmin, (pmax+1), 1):
		if i ==page :
			out += '<li class="page-item active"><a class="page-link">'+str(i)+'</a></li>'
		else:
			if i==1:
				out += '<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="'+fuc_load+'(1);">'+str(i)+'</a></li>'
			else:
				out += '<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="'+fuc_load+'('+str(i)+');">'+str(i)+'</a></li>'

	# interval
	if page < (tpages-adjacents-1):
		out += '<li class="page-item disabled"><a class="page-link">...</a></li>'

	# last
	if page < (tpages -adjacents):
		out += '<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="'+fuc_load+'('+str(tpages)+');">'+str(tpages)+'</a></li>'

	# next
	if page < tpages:
		out += '<li class="page-item"><a class="page-link" href="javascript:void(0);"" onclick="'+fuc_load+'('+str(page+1)+');">'+nextlabel+'</a></li>'
	else:
		out += '<li class="page-item disabled"><a class="page-link">'+nextlabel+'</a></li>'

	out += '</ul></nav>'
	return out
